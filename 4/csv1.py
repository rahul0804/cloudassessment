import csv
#creating csv file
rows=[ ['Nikhil', 'COE', '2', '9.0'],
         ['Sanchit', 'COE', '2', '9.1'],
         ['Aditya', 'IT', '2', '9.3'],
         ['Sagar', 'SE', '1', '9.5'],
         ['Prateek', 'MCE', '3', '7.8'],
         ['Sahil', 'EP', '2', '9.1']]
with open("sample.csv",'w') as a:
    s=csv.writer(a)
    s.writerows(rows)
a.close()

# Reading the sample file
output=[['Rahul', 'CE0', '2', '15.0']] # Adding rows in a output file(for difference)
with open("sample.csv",'r') as r:
    reader=csv.reader(r)
    for i in reader:
        output.append(i)
r.close()

#writing in an output file

with open("output.csv",'w') as o:
    writer=csv.writer(o)
    writer.writerows(output)
o.close()        


print("done")