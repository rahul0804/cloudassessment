from django.db import models
from django.db.models.lookups import BuiltinLookup

# Create your models here.
class book(models.Model):
    book_name=models.CharField(max_length=120)
    author_name=models.CharField(max_length=120)
    book_id=models.CharField(max_length=50)
    description=models.CharField(max_length=150,blank=True)
    def __str__(self):
        return str(self.book_id)+"-"+str(self.book_name)
    class Meta:
        unique_together=["book_id"]    
class user(models.Model):
    user_name=models.CharField(max_length=120)
    user_id=models.CharField(max_length=130)
    pass_word=models.CharField(max_length=10)

    def __str__(self):
        return self.user_name
    class Meta:
        unique_together=["user_id"]           

class book_taken(models.Model):
    book=models.ForeignKey(
        book, on_delete=models.CASCADE)
    user=models.ForeignKey(
        user, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.book)+"--"+str(self.user)


