from django.contrib import admin

# Register your models here.
from .models import book, book_taken, user

admin.site.register(book)
admin.site.register(user)
admin.site.register(book_taken)