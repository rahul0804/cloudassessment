from django.urls import path
from user import views

urlpatterns=[
    path('',views.login),
    path('userdetails/',views.user_details),
    path('userpass/',views.user_password),
    path('verify/',views.validate)
    ]

