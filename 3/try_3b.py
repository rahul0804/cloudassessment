s=[1]
try:
    if s[0]:
        print("Try Block")
        try:
            if s[1]:
                print("Nested Try Block")
        except:
            print("Nested Except Block")
except:
    print("Except Block")        
finally:
    print("Finally block")            