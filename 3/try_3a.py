# case1
s=[]
try:
    if s[0]:
        print("yes,try block worked")
except:
    print("Yes,Except block worked")
finally:
    print("Yes,Finally block worked")


print("-"*30)
# case 2            
s=[1]
try:
    if s[0]:
        print("Yes,try block worked")
except:
    print("Yes,Except block worked")
finally:
    print("Yes,Finally block worked")

